import React from 'react';
import { MessageItem } from '../../containers/Chat';
import { Header, Icon, Popup, Responsive } from 'semantic-ui-react';
import style from './style.module.css';
import ChatStatistic from '../ChatStatistic';

interface ChatHeaderProps {
    chatName: string;
    participants: number;
    messages: MessageItem[];
}

const ChatHeader: React.FC<ChatHeaderProps> = ({
    chatName,
    participants,
    messages,
}: ChatHeaderProps) => (
    <div className={style.chatHeader}>
        <div className={style.chatNameWrapper}>
            <Header as="h2" content={chatName} className={style.chatName} />
        </div>
        <div>
            <Responsive as={React.Fragment} minWidth={605}>
                <ChatStatistic participants={participants} messages={messages} />
            </Responsive>
            <Responsive as={React.Fragment} maxWidth={604.9}>
                <Popup
                    size="tiny"
                    hoverable
                    content={<ChatStatistic participants={participants} messages={messages} />}
                    position="bottom right"
                    trigger={<Icon name="info circle" size="big" style={{ color: '#f7a232' }} />}
                    style={{ borderRadius: '20px' }}
                />
            </Responsive>
        </div>
    </div>
);

export default ChatHeader;
