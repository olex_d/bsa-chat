import React, { useEffect, useState } from 'react';
import { Button, Form, Label, Responsive, TextArea } from 'semantic-ui-react';
import style from './style.module.css';
import { EditMessageItem } from '../../containers/Chat';

interface ChatInputProps {
    saveMessage: (userId: string, text: string) => void;
    editMessageItem: EditMessageItem | null;
}

const ChatInput: React.FC<ChatInputProps> = ({ saveMessage, editMessageItem }: ChatInputProps) => {
    const [text, setText] = useState('');

    useEffect(() => {
        if (editMessageItem !== null) {
            setText(editMessageItem.text);
        }
    }, [editMessageItem]);

    const handleSaveMessage = (): void => {
        saveMessage(process.env.REACT_APP_OWNER_USER_ID, text);
        setText('');
    };

    return (
        <div className={style.messageFormWrapper}>
            <Form onSubmit={handleSaveMessage} className={style.messageForm}>
                {editMessageItem !== null && <Label style={{ fontStyle: 'italic' }}>editing</Label>}
                <div className={text.length > 0 ? style.messageFormGrid : 'hidden'}>
                    <TextArea
                        placeholder="Enter your message"
                        value={text}
                        onChange={(e: React.FormEvent<HTMLTextAreaElement>): void =>
                            setText((e.target as HTMLTextAreaElement).value)
                        }
                        className={style.messageTextArea}
                        rows={4}
                    />

                    {text.length > 0 && (
                        <>
                            <Responsive as={React.Fragment} minWidth={705}>
                                <Button
                                    type="submit"
                                    content="Send"
                                    icon="arrow alternate circle up"
                                    labelPosition="left"
                                    className={style.sendButton}
                                    color="orange"
                                />
                            </Responsive>
                            <Responsive as={React.Fragment} maxWidth={704.9}>
                                <Button
                                    type="submit"
                                    icon="arrow alternate circle up"
                                    className={style.sendButton}
                                    color="orange"
                                />
                            </Responsive>
                            <Responsive as={React.Fragment} minWidth={705}>
                                <Button
                                    type="submit"
                                    content="Clear"
                                    icon="delete"
                                    labelPosition="left"
                                    className={style.clearButton}
                                    basic
                                />
                            </Responsive>
                            <Responsive as={React.Fragment} maxWidth={704.9}>
                                <Button
                                    type="submit"
                                    icon="delete"
                                    className={style.clearButton}
                                    size="tiny"
                                    basic
                                />
                            </Responsive>
                        </>
                    )}
                </div>
            </Form>
        </div>
    );
};

export default ChatInput;
