import React from 'react';
import { EditMessageItem, MessageItem } from '../../containers/Chat';
import { Comment as MessageUI, Label } from 'semantic-ui-react';
import style from './style.module.css';
import { extractTimeFormDateTime } from '../../utils/DateTimeFormatter';

interface MessageProps {
    message: MessageItem;
    likeMessage: (userId: string, messageId: string) => void;
    editMessage: (message: EditMessageItem) => void;
    deleteMessage: (messageId: string) => void;
}

const Message: React.FC<MessageProps> = ({
    message,
    likeMessage,
    editMessage,
    deleteMessage,
}: MessageProps) => (
    <>
        <MessageUI
            className={
                message.userId !== process.env.REACT_APP_OWNER_USER_ID
                    ? style.message
                    : style.ownMessage
            }
        >
            {message.userId !== process.env.REACT_APP_OWNER_USER_ID && (
                <MessageUI.Avatar src={message.avatar} />
            )}
            <MessageUI.Content>
                {message.userId !== process.env.REACT_APP_OWNER_USER_ID && (
                    <MessageUI.Author as="a" content={message.user} />
                )}
                <MessageUI.Metadata>
                    {extractTimeFormDateTime(message.createdAt)}{' '}
                    {message.editedAt !== null &&
                        new Date(message.editedAt).toString() !== 'Invalid Date' && (
                            <span>(edited)</span>
                        )}
                </MessageUI.Metadata>

                <MessageUI.Text content={message.text} />
                <MessageUI.Actions>
                    <>
                        <Label
                            basic
                            size="small"
                            as="a"
                            className={`${style.toolbarBtn} ${
                                message.userId === process.env.REACT_APP_OWNER_USER_ID
                                    ? style.disabledButton
                                    : style.likeButton
                            }`}
                            icon="thumbs up"
                            content={message.likes}
                            onClick={(): void =>
                                likeMessage(process.env.REACT_APP_OWNER_USER_ID, message.id)
                            }
                        />
                        {message.userId === process.env.REACT_APP_OWNER_USER_ID && (
                            <span className={style.ownMessageActions}>
                                <Label
                                    basic
                                    size="small"
                                    as="a"
                                    className={`${style.toolbarBtn} ${style.editButton}`}
                                    icon="pencil"
                                    onClick={(): void => {
                                        editMessage({ id: message.id, text: message.text });
                                    }}
                                />
                                <Label
                                    basic
                                    size="small"
                                    as="a"
                                    className={`${style.toolbarBtn} ${style.deleteButton}`}
                                    icon="trash"
                                    onClick={(): void => deleteMessage(message.id)}
                                />
                            </span>
                        )}
                    </>
                </MessageUI.Actions>
            </MessageUI.Content>
        </MessageUI>
    </>
);

export default Message;
