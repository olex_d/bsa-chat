import { MessageItem, User } from './index';

export const getUsers = (messages: MessageItem[]): User[] => {
    const messagesFromUniqueUsers = messages.filter(
        (message, i, arr) => arr.findIndex((m) => m.userId === message.userId) === i,
    );
    return messagesFromUniqueUsers.map((message) => ({
        id: message.userId,
        username: message.user,
        avatar: message.avatar,
    }));
};

export const addLikeProperty = (messages: MessageItem[]): MessageItem[] => {
    return messages.map((message) => ({ ...message, likes: 0 }));
};
