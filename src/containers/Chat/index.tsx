import React, { useEffect, useState } from 'react';
import Spinner from '../../components/Spinner';
import ChatHeader from '../../components/ChatHeader';
import ChatBody from '../ChatBody';
import ChatInput from '../../components/ChatInput';
import { generateUUID } from '../../utils/UUIDService';
import { addLikeProperty, getUsers } from './actions';

export interface MessageItem {
    id: string;
    text: string;
    user: string;
    avatar: string;
    userId: string;
    editedAt: Date | null;
    createdAt: Date;
    likes: number;
}

export interface EditMessageItem {
    id: string;
    text: string;
}

export interface User {
    id: string;
    username: string;
    avatar: string;
}

interface UserLike {
    userId: string;
    messageId: string;
}

const Chat: React.FC = () => {
    const [isLoading, setLoading] = useState(true);
    const [messages, setMessages] = useState<MessageItem[]>([]);
    const [users, setUsers] = useState<User[]>([]);
    const [usersLikes, setUsersLikes] = useState<UserLike[]>([]);

    const [editMessageItem, setEditMessageItem] = useState<EditMessageItem | null>(null);

    useEffect(() => {
        if (messages === undefined || messages?.length === 0) {
            fetch(process.env.REACT_APP_MESSAGES_LIST_API)
                .then((res) => res.json())
                .then((response) => {
                    let apiMessages = response as MessageItem[];
                    apiMessages = addLikeProperty(apiMessages);

                    setMessages(apiMessages);
                    setUsers(getUsers(apiMessages));
                    setLoading(false);
                })
                .catch((err) => console.error(err));
        }
    }, [messages]);

    const saveMessage = (userId: string, text: string): void => {
        if (text === undefined || text.trim().length === 0 || text === editMessageItem?.text) {
            setEditMessageItem(null);
            return;
        }

        const clonnedMessages = [...messages];
        if (editMessageItem !== undefined && editMessageItem !== null) {
            const messageIndex = clonnedMessages.findIndex(
                (message) => message.id === editMessageItem.id,
            );
            if (messageIndex === -1) return;

            clonnedMessages[messageIndex].editedAt = new Date();
            clonnedMessages[messageIndex].text = text;

            setMessages(clonnedMessages);
            setEditMessageItem(null);
        } else {
            const author = users.find((user) => user.id === userId);
            if (author === undefined || author === null) return;

            const newMessage: MessageItem = {
                id: generateUUID(),
                text: text,
                user: author.username,
                avatar: author.avatar,
                userId: author.id,
                editedAt: null,
                createdAt: new Date(),
                likes: 0,
            };

            clonnedMessages.push(newMessage);
            setMessages(clonnedMessages);
        }
    };

    const editMessage = (message: EditMessageItem): void => {
        setEditMessageItem(message);
    };

    const deleteMessage = (messageId: string): void => {
        if (messages === undefined) return;

        const clonnedMessages = [...messages];
        const messageIndex = clonnedMessages.findIndex((message) => message.id === messageId);
        if (messageIndex === -1) return;

        clonnedMessages.splice(messageIndex, 1);
        setMessages(clonnedMessages);
    };

    const likeMessage = (userId: string, messageId: string): void => {
        if (messages === undefined || usersLikes === undefined) return;

        const clonnedMessages = [...messages];
        const messageIndex = clonnedMessages.findIndex((message) => message.id === messageId);
        if (messageIndex === -1) return;

        const clonnedUsersLikes = [...usersLikes];
        const likeIndex = clonnedUsersLikes.findIndex(
            (ul) => ul.userId === userId && ul.messageId === messageId,
        );

        if (likeIndex !== -1) {
            clonnedMessages[messageIndex].likes -= 1;
            clonnedUsersLikes.splice(likeIndex, 1);
        } else {
            clonnedMessages[messageIndex].likes += 1;
            clonnedUsersLikes.push({ userId, messageId });
        }

        setMessages(clonnedMessages);
        setUsersLikes(clonnedUsersLikes);
    };

    return (
        <>
            {isLoading || messages === undefined ? (
                <Spinner />
            ) : (
                <div>
                    <ChatHeader
                        chatName={'Lampoviy BSA Chatik'}
                        participants={users.length}
                        messages={messages}
                    />
                    <ChatBody
                        messages={messages}
                        likeMessage={likeMessage}
                        editMessage={editMessage}
                        deleteMessage={deleteMessage}
                    />
                    <ChatInput saveMessage={saveMessage} editMessageItem={editMessageItem} />
                </div>
            )}
        </>
    );
};

export default Chat;
