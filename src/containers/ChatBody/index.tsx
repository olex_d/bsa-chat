import React, { useEffect, useLayoutEffect, useState } from 'react';
import { EditMessageItem, MessageItem } from '../Chat';
import { Comment as MessageUI, Divider, Grid, Header, Icon } from 'semantic-ui-react';
import EmptyChatBody from '../../components/EmptyChatBody';
import Message from '../../components/Message';
import style from './style.module.css';
import { extractDateFromDatetime } from '../../utils/DateTimeFormatter';

interface ChatBodyProps {
    messages: MessageItem[];
    likeMessage: (userId: string, messageId: string) => void;
    editMessage: (message: EditMessageItem) => void;
    deleteMessage: (messageId: string) => void;
}

let firstMessageDate = new Date().getDate();
const isSeparatorNeeded = (messageDate: Date): boolean => {
    const messageDateNum = new Date(messageDate).getDate();
    if (messageDateNum !== firstMessageDate) {
        firstMessageDate = messageDateNum;
        return true;
    }
    return false;
};

const ChatBody: React.FC<ChatBodyProps> = ({
    messages,
    likeMessage,
    editMessage,
    deleteMessage,
}: ChatBodyProps) => {
    const [messagesCount, setMessagesCount] = useState(0);
    const [windowWidth, setWindowWidth] = useState(1080);

    const messagesBottomRef = React.useRef<HTMLDivElement>(null);

    useLayoutEffect(() => {
        setWindowWidth(window.innerWidth);

        // appears already scrolled
        if (messagesBottomRef.current !== null) {
            messagesBottomRef.current.scrollIntoView();
        }
    }, []);

    useEffect(() => {
        if (messagesBottomRef.current !== null && messages.length !== messagesCount) {
            messagesBottomRef.current.scrollIntoView({ behavior: 'smooth' });
            setMessagesCount(messages.length);
        }
    }, [messages]);

    useEffect(() => {
        window.addEventListener('resize', () => {
            setWindowWidth(window.innerWidth);
        });
    }, [window]);

    return (
        <div className={style.chatBodyWrapper}>
            {messages?.length === 0 ? (
                <EmptyChatBody />
            ) : (
                <MessageUI.Group className={style.chatBody}>
                    <Grid columns={windowWidth <= 760 ? 1 : 2} className={style.messagesGrid}>
                        <Grid.Row />
                        {messages
                            .sort(
                                (m1, m2) =>
                                    new Date(m1.createdAt).getTime() -
                                    new Date(m2.createdAt).getTime(),
                            )
                            .map((message) => (
                                <React.Fragment key={message.id}>
                                    {isSeparatorNeeded(message.createdAt) && (
                                        <Divider horizontal className={style.messagesDelimiter}>
                                            <Header as="h4">
                                                <Icon name="angle down" />
                                                {extractDateFromDatetime(message.createdAt)}
                                            </Header>
                                        </Divider>
                                    )}
                                    <Grid.Row>
                                        {message.userId === process.env.REACT_APP_OWNER_USER_ID ? (
                                            <Grid.Column floated="right" textAlign="right">
                                                <Message
                                                    message={message}
                                                    likeMessage={likeMessage}
                                                    editMessage={editMessage}
                                                    deleteMessage={deleteMessage}
                                                />
                                            </Grid.Column>
                                        ) : (
                                            <Grid.Column floated="left">
                                                <Message
                                                    message={message}
                                                    likeMessage={likeMessage}
                                                    editMessage={editMessage}
                                                    deleteMessage={deleteMessage}
                                                />
                                            </Grid.Column>
                                        )}
                                    </Grid.Row>
                                </React.Fragment>
                            ))}
                        <div ref={messagesBottomRef} />
                    </Grid>
                </MessageUI.Group>
            )}
        </div>
    );
};

export default ChatBody;
