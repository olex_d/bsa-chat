import React from 'react';
import ReactDOM from 'react-dom';
import 'semantic-ui-css/semantic.min.css';
import Chat from './containers/Chat';
import PageHeader from './components/PageHeader';
import { Container } from 'semantic-ui-react';
import './index.css';

ReactDOM.render(
    <React.StrictMode>
        <Container>
            <PageHeader />
            <Chat />
        </Container>
    </React.StrictMode>,
    document.getElementById('root'),
);
