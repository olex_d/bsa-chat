/// <reference types="react-scripts" />

declare global {
    namespace NodeJS {
        interface ProcessEnv {
            REACT_APP_MESSAGES_LIST_API: string;
            REACT_APP_OWNER_USER_ID: string;
        }
    }
}

export {};
